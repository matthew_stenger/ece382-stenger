;**********************************************************************
;
; COPYRIGHT 2016 United States Air Force Academy All rights reserved.
;
; United States Air Force Academy     __  _______ ___    _________
; Dept of Electrical &               / / / / ___//   |  / ____/   |
; Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
; 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
; USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
;
; ----------------------------------------------------------------------
;
;   FILENAME      : main.asm
;   AUTHOR(S)     : Matthew Stenger
;   DATE          : 9/13/2018
;   COURSE		  : ECE 382
;
;   Lab 1
;
;   DESCRIPTION   : This code simply calculates values given input
;
;   DOCUMENTATION : None
;
; Academic Integrity Statement: I certify that, while others may have
; assisted me in brain storming, debugging and validating this program,
; the program itself is my own work. I understand that submitting code
; which is the work of other individuals is a violation of the honor
; code.  I also understand that if I knowingly give my original work to
; another individual is also a violation of the honor code.
;
;**********************************************************************
;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.
;myProgram:    .byte  0x11, 0x11, 0x11, 0x11, 0x11, 0x44, 0x22, 0x22, 0x22, 0x11, 0xCC, 0x55
;myProgram:    .byte  0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0xDD, 0x44, 0x08, 0x22, 0x09, 0x44, 0xFF, 0x22, 0xFD, 0x55
;myProgram:	   .byte 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55
myProgram:	   .byte  0xFF, 0x33, 0xFF, 0x44, 0xEE, 0x22, 0xFF, 0x33, 0xFF, 0x55

              .data

myResults:		.space      20                ; reserving 20 bytes of space

ADD_OP:       	.equ        0x11
SUB_OP:         .equ        0x22
MUL_OP:         .equ        0x33
CLR_OP:         .equ        0x44
END_OP:         .equ        0x55

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			mov.w   #0xC000, r4				; Store location of current program input
			mov.w	#0x0200, r8				; Store location of results in memory
											; r5 - first operand
											; r6 - operation
											; r7 - second operand
			mov.b	@r4+, r5					; get first operand, store in r5, increment
get_next_op mov.b   @r4+, r6		 		; Get first operation, store in r6,  increment

start_check cmp.b   #CLR_OP, r6				; Check for clr_op
			jnz		endop_check				; Jump to next check
			call 	#clr_op					; Was clr_op, execute
			jmp		get_next_op				; Get new operation

endop_check cmp.b   #END_OP, r6				; Check for end_op
            jnz		addop_check				; Jump to addop_check
            call	#end_op					; Was end_op, execute

addop_check mov.b   @r4+, r7				; Get second operand, store in r7, increment
            cmp.b	#ADD_OP, r6				; Check for add_op
            jnz		subop_check				; Jump to subop_check
            call	#add_op					; Was add_op, execute
            jmp 	get_next_op				; Get new operation

subop_check cmp.b	#SUB_OP, r6				; Check for sub_op
            jnz		mulop_check				; Jump to mulop_check
            call	#sub_op					; Was sub_op, execute
            jmp		get_next_op				; Get new operation

mulop_check cmp.b	#MUL_OP, r6				; Check for mul_op - should be at this point
            jnz		endop_call				; Wasn't mul_op, oh no
            call	#mul_op					; Was mul_op, execute
            jmp 	get_next_op				; Get new operatoin

endop_call	call    end_op					; Should never reach here


add_op:		add.b   r7, r5					; Add the two operands, assume unsigned
			jnc		store1					; No carry, store
            call	#set_max					; Was a carry, set max
store1		call    #store_rslt				; Call set_rslt
			ret								; return


sub_op:     sub.b   r7, r5					; Subtract the two operands, assume unsigned
			clrc
			jn		set_minL				; Check for negative (difference < 0), set min
			jnc		store2					; Check for overflow
set_minL	call	#set_min				; Was neg/too big
store2		call	#store_rslt				; Jump to set_rslt
			ret


mul_op:     clr		r10						; Clear counter
			clr		r11						; Clear summation
			mov.b   r5, r10					; Move r5 into r10
mul_loop	add.b	r7, r11					; Add to summation
			jc		set_maxL				; Check for carry (sum > 255), set max
			dec.b   r10						; Decrement
			cmp     #0, r10					; Is counter over?
			jnz		mul_loop				; Continue loop
			mov.b	r11, r5					; Move summation to proper location
			call	#store_rslt				; Jump to store
			ret								; return
set_maxL    call	#set_max
			call	#store_rslt				; Jump to store
			ret


set_max:    mov.b   #0xFF, r5				; Set result to 255
			ret								; return


set_min:    mov.b   #0x00, r5				; Set result to 0
			ret								; return


store_rslt: clr.b	0(r8)					; Clear the memory location
			mov.b   r5, 0(r8)      			; Move result to memory
			inc		r8						; Increment r8
            ret								; return


clr_op:
			clr.b 	0(r8)					; Clear memory and increment
			inc		r8						; Increment r8
			mov.b   @r4+, r5			 	; Get first operand, store in r5,  increment
			ret								; return


end_op:
cpu_trap	jmp		cpu_trap				; It's a trap!
			ret


;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
