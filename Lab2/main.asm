;**********************************************************************
;
; COPYRIGHT 2016 United States Air Force Academy All rights reserved.
;
; United States Air Force Academy     __  _______ ___    _________
; Dept of Electrical &               / / / / ___//   |  / ____/   |
; Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
; 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
; USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
;
; ----------------------------------------------------------------------
;
;   FILENAME      : main.asm
;   AUTHOR(S)     : Matthew Stenger
;   DATE          : 9/24/2018
;   COURSE		  : ECE 382
;
;   Lab 2
;
;   DESCRIPTION   : This code simply toggles between 4 LEDs
;
;   DOCUMENTATION : None
;
; Academic Integrity Statement: I certify that, while others may have
; assisted me in brain storming, debugging and validating this program,
; the program itself is my own work. I understand that submitting code
; which is the work of other individuals is a violation of the honor
; code.  I also understand that if I knowingly give my original work to
; another individual is also a violation of the honor code.
;
;**********************************************************************
;-------------------------------------------------------------------------------
; MSP430 Assembler Code Template for use with TI Code Composer Studio
;
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------

			bic.b	#BIT3, &P1DIR			; Set P1.3 to input
			bis.b	#BIT3, &P1REN			; Enable P1.3 resistor
			bis.b	#BIT3, &P1OUT			; Set as pull down resistor

			bis.b	#BIT7, &P1DIR			; Set P1.7 to output (RCLK)
			bic.b	#BIT7, &P1OUT			; Set as low

			bis.b	#BIT4, &P2DIR			; Set P2.4 to output (SRCLK)
			bic.b	#BIT4, &P2OUT			; Set clock as low

			bis.b	#BIT5, &P2DIR			; Set P2.5 to output (SER)
			bic.b	#BIT5, &P2OUT			; Set input as low

			mov		#0x0, r12				; Current LED lit
											; (0 none, 1 Qa, 2 Qb, etc.)

check_btn:
			bit.b	#BIT3, &P1IN			; Test if pushed
			jnz		check_btn				; Not pushed, continue checking
											; Button pushed, useful stuff...

			inc		r12						; Increment r12 for next LED
			cmp		#0x5, r12				; Are we invalid?
			jnz		valid_LED				; LED is valid
			mov		#0x1, r12				; Cycle back to LED 1
valid_LED:
			cmp		#0x1, r12				; Are we turning on the first LED?
			jnz		not_LED1				; No, need to push 0 to SER
			bis.b	#BIT5, &P2OUT			; Yes, push a 1 to SER
			jmp		send_sig				; Jump to send signal
not_LED1:	bic.b	#BIT5, &P2OUT			; Push a 0 to SER
send_sig:	bis.b	#BIT4, &P2OUT			; Update LEDs (write to storage)
			bic.b	#BIT4, &P2OUT			; Reset clock (back to low)
			bis.b 	#BIT7,&P1OUT			; Copy values to shift register
			bic.b	#BIT7, &P1OUT			; Reset RCLK (back to low)

			call 	#sw_delay				; Call SW delay (debounce)
btn_pushed:	bit.b	#BIT3, &P1IN			; Recheck for continued press
			jz		btn_pushed				; Button still pushed, keep checking

			call	#sw_delay				; Button released, call SW delay
			jmp		check_btn				; Loop back to check for press

;-------------------------------------------------------------------------------
; Software delay
; Purpose:  Delays code based on value of R5
;	   R5 is currently hard coded as 0x63
; Destroyed: None
; Returned: None
;-------------------------------------------------------------------------------
sw_delay:									; call sw_delay					(5)
			push 	r5						; Do not destroy r5				(3)
			mov	 	#0x63, r5				; Counter for SW delay			(2)
delay:		dec	 	r5                      ; Decrement counter				(1)
			jnz		delay					; Loop decrement if not zero	(2)
			pop		r5						; Return r5 to original value	(2)
			ret								; Return						(3)
; Total delay: 5 + 3 + 2 + (0x8 * (1 + 2)) + 2 + 3 = 312 cycles
; 312 cycles / 1039636.127 Hz = 300.105 micro seconds
; End Software delay -----------------------------------------------------------

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
