/*--------------------------------------------------------------------
Name: Matthew Stenger
Date: 7 Nov 18
Course: ECE 382 - Embedded Computer Systems I
File: main.c
Event: Lab 3 - Interrupts - �Remote Control Decoding�

Purp: Reverse engineer (hack) an IR remote control to light up some LEDs

Doc:    None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include <stdint.h>
#include <stdbool.h>

// delay code written in assembly
// make sure to drag/drop the assembly code too
extern void Delay160ms(void);
extern void Delay40ms(void);

// macros to make life easier
#define        IR_PIN            (P2IN & BIT6)
#define        HIGH_2_LOW        P2IES |= BIT6
#define        LOW_2_HIGH        P2IES &= ~BIT6

// Globals are bad, but these are constants and can't be changed accidently.
// Set them up for your remote.
const uint32_t Button1 = 0x30DFA05F;
const uint32_t Button2 = 0x30DF609F;
const uint32_t Button3 = 0x30DFE01F;
const uint32_t Button4 = 0x30DF10EF;
const uint32_t Button5 = 0x30DF906F;

// global irPacket for capturing buttons from interrupt
uint32_t irPacket;
uint8_t irPacketCount = 0;

// function declarations
void clearPacketAndResetCount(void);
void button1Pressed(void);
void button2Pressed(void);
void button3Pressed(void);
void button4Pressed(void);
void button5Pressed(uint8_t LEDsON);

// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void main(void){

    WDTCTL=WDTPW+WDTHOLD;  // stop WDT

    BCSCTL1 = CALBC1_8MHZ;
    DCOCTL = CALDCO_8MHZ;

    // Set up P2.6 as GPIO not XIN
    // as before
    P2DIR &= ~BIT6;				// use P2.6 as input
    P2SEL &= ~BIT6;
    P2SEL2 &= ~BIT6;
	P2REN |= BIT6;				// enable pull/down
	P2OUT |= BIT6;				// set as pull up

    HIGH_2_LOW;         		// check the header out.  P2IES changed.

	P2IFG &= ~BIT6;				// clear P2.6 interrupt flag
	P2IE |= BIT6;				// enable interrupts for this pin

	// Set LEDs as outputs
	// And turn the LEDs off
	// ... you will need several lines to

    P1DIR |= BIT6;				// set LEDs to output
    P1DIR |= BIT0;

    P1OUT &= ~BIT6;				// turn LEDs off
    P1OUT &= ~BIT0;

    // set LED outputs
    P2DIR |= BIT3 | BIT4;		// Use P2.3 & P2.4 for this
    P2OUT &= ~(BIT3 |BIT4);

	// create a 16ms roll-over period
	// clear flag before enabling interrupts = good practice
	// Use 1:8 prescalar off SMCLK and enable interrupts
	TA0CTL |= TASSEL_2 | ID_3 | MC_1 | TACLR;
	TA0CCR0 = 0x3E80;

     // and enable interrupts
    __enable_interrupt();		// enable interrupts

    // toggle the Green led for on for 1 second
    P1OUT ^= BIT6;				// turn on green LED
    __delay_cycles(8000000);	// wait 1 second
    P1OUT ^= BIT6;				// turn back off
    uint8_t LEDsON = 0;

    // main loop
    while(1){
        // check for valid ir packet and handle it
        // however you are supposed to
    	if(irPacket == Button1){ // call button 1 function
    		button1Pressed();

    	} else if(irPacket == Button2){ // call button 2 function
    		button2Pressed();

    	} else if(irPacket == Button3){ // call button 3 function
    		button3Pressed();

    	} else if(irPacket == Button4){ // call button 4 function
    		button4Pressed();

    	} else if(irPacket == Button5){ // call button 5 function
    		button5Pressed(LEDsON);
    		if(LEDsON == 1){
    			LEDsON = 0;
    		} else {
    			LEDsON = 1;
    		}

    	} else {  // did not match any button, check for error
    		if(irPacketCount == 32){ // do we have 32 bits?
    			if((irPacket != Button5) && (irPacket != Button4) && (irPacket != Button3)
    					&& (irPacket != Button2) && (irPacket != Button1)){ // is it one of our buttons?
    				clearPacketAndResetCount(); // clear packet
    			}
    		}
    	}


    } // end infinite loop
} // end main

void clearPacketAndResetCount(void){
	__delay_cycles(1421120);	// wait for extra IR packets
	irPacket = 0x0;				// clear the irPacket
	irPacketCount = 0; 			// reset the count
}

// Pressing button 1 should light up LED 1
void button1Pressed(void){
	P2OUT |= BIT3;
	clearPacketAndResetCount(); // clear packet
}

// Pressing button 2 should turn off LED 1
void button2Pressed(void){
	P2OUT &= ~BIT3;
	clearPacketAndResetCount(); // clear packet
}

// Pressing button 3 should blink LED 2 three times (about 1 second on and 1 second off for each blink)
void button3Pressed(void){
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	clearPacketAndResetCount(); // clear packet
}

// Pressing button 4 should blink LED 2 five times (same timings as button 3)
void button4Pressed(void){
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	__delay_cycles(8000000);    // wait 1 second
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT |= BIT4;
	__delay_cycles(8000000);	// wait 1 second
	P2OUT &= ~BIT4;
	clearPacketAndResetCount(); // clear packet
}

// Pressing button 5 should turn on/off LED 1 and 2
void button5Pressed(uint8_t LEDsON){
	if(LEDsON){	// turn LEDs off if they were on
		P2OUT &= ~(BIT3 | BIT4);
	} else{						// turn LEDs on if they were off
		P2OUT |= BIT3 | BIT4;
	}

	clearPacketAndResetCount(); // clear packet
}


// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR
__interrupt void pinChange (void) {
    uint8_t    pin;
    uint16_t   pulseDuration;	// The timer is 16-bits

    if (IR_PIN)  // is pin high or low?
        pin=1;
    else
        pin=0;

    switch (pin) {
        case 0: // !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
        	P1OUT ^= BIT0;					// Toggle red LED on pin change
            pulseDuration = TA0R;

            // figure out what this is?
            // start? 0-bit? 1-bit?
            if(irPacketCount < 32){
				if(((4374 - 100) < pulseDuration) && (pulseDuration < (4374 + 100))){	// Start logic 1 pulse
					// Start bit- not used currently
				} else if(((526 - 50) < pulseDuration) && (pulseDuration < (526 + 50))){ // Data 0, 1 pulse
					irPacket = irPacket * 2;			// Logic shift left by 1 (<< was not working for some reason)
					irPacketCount = irPacketCount + 1;	// increment the count
				} else if(((1625 - 50) < pulseDuration) && (pulseDuration < (1625 + 50))){ // Data 1, 1 pulse
					irPacket = irPacket * 2;			// Logic shift left by 1 (<< was not working for some reason)
					irPacket = irPacket + 1;			// add the 1 bit
					irPacketCount = irPacketCount + 1;	// increment the count
				} else if(((39608 - 100) < pulseDuration) && (pulseDuration < (39608 + 100))){ // Stop 1, 1 pulse
					// Stop bit- not currently used
				}
            } else if (irPacketCount == 32){
            	P1OUT |= BIT6;				// Toggle green LED when full packet received
            	__delay_cycles(4000000);	// wait 1 second
            	P1OUT &= ~BIT6;
            }

            TA0CTL &= ~(MC_1);
            LOW_2_HIGH;             		// Set up pin interrupt on positive edge
            break;

        case 1: // !!!!!!!!POSITIVE EDGE!!!!!!!!!!!

            TA0R = 0x0000;          // time measurements are based at time 0
            TA0CTL |= MC_1 | TAIE;
            HIGH_2_LOW;             // Set up pin interrupt on falling edge
            break;
    } // end switch

    // Clear the interrupt flag to prevent
    // immediate ISR re-entry
    P1OUT &= ~BIT0;					// turn off the red LED
    P2IFG &= ~BIT6;					//clear interrupt flag

} // end pinChange ISR


#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflow (void) {
    TA0CTL &= ~MC_1;   // Turn off Timer A and Enable Interrupt
    TA0CTL &= ~TAIE;
    TA0CTL &= ~TAIFG;  // clear timer a interrupt
}
