/*--------------------------------------------------------------------
Name: Matthew Stenger
Date: 19 Nov 18
Course: ECE 382 - Embedded Computer Systems I
File: main.c
Event: Lab 4 - Servo Motor

Purp: Interfacing a microcontroller with external HW

Doc:    None.

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430.h> 
#include <stdint.h>
#include <stdbool.h>

void uart_putc(const char * c, const int8_t len);
void uart_putcc(const int8_t i);
void itoa(int16_t value, char* result, uint16_t base);
void updateServoAngleDisplay();
void updateADCDisplay();
int16_t servoAngle = 0;
int16_t ADCvalue;
char result[3];
char ADCresult[3];

int main(void) {
    	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	
    	/* Use Calibration values for 1MHz Clock DCO*/
        DCOCTL = 0;
        BCSCTL1 = CALBC1_1MHZ;
        DCOCTL = CALDCO_1MHZ;

        // Set P1SEL/P1SEL2 for P1.1 and P1.2 for the UCA0
        P1SEL |= BIT1 | BIT2;
        P1SEL2 |= BIT1 | BIT2;

        // set up UART
        UCA0CTL1 = UCSWRST;    // Hold the USCI

        UCA0CTL1 |= UCSSEL_2;  // select BRCLK = SMCLK
        UCA0CTL0 = 0;          // 8N1
        UCA0BR0 = 104;         // p 424 1MHz@9600
        UCA0BR1 = 0;           // p 424 1MHz@9600
        UCA0MCTL = UCBRS0;     // UCBRSx=1

        // Take the USCI out of reset
        UCA0CTL1 &= ~UCSWRST;

        // set up ADC
        ADC10CTL0 = ADC10SHT_3 + ADC10ON + ADC10IE;  // ADC10ON, interrupt enabled
        ADC10CTL1 = INCH_4;                       	 // input A4
        ADC10AE0 |= BIT4;                        	 // P1.4 ADC Analog enable
        ADC10CTL1 |= ADC10SSEL1|ADC10SSEL0;      	 // Select SMCLK

        // LEDs for debugging
        //P1DIR |= BIT0;      // Set P1.0 to output direction
        //P1DIR |= BIT6;      // Set P1.6 to output direction

        P2DIR |= BIT4;		// use P2.4 for PWM signal to servo
        P2OUT &= ~BIT4;		// clear the output

        // set up PWM
        P1DIR |= BIT6;          // TA0CCR1 on P1.6
		P1SEL |= BIT6;          // TA0CCR1 on P1.6
		TA0CTL &= ~MC1|MC0;     // stop timer A0
		TA0CTL |= TACLR;        // clear timer A0
		TA0CTL |= TASSEL1;      // configure for SMCLK
		TA0CCR0 = 20000;   		// set signal period to 20000 clock cycles (~20 milliseconds)
		TA0CCR1 = 1500;         // set duty cycle to 1500/20000 (7.5%)
		TA0CCTL1 |= OUTMOD_7;   // set TACCTL1 to Reset / Set mode
		TA0CTL |= MC0;          // count up

        // send control character
		uart_putcc(0xFE); // 241
		// send clear character
		uart_putcc(0x01); // 1

        // display my information
		uart_putc("ECE 382, Fall 2018  ", 20);
		uart_putc("C1C Matthew Stenger", 19);
		__delay_cycles(3000000);

       	// send control character
		uart_putcc(0xFE); // 241
		// send clear character
		uart_putcc(0x01); // 1

		__delay_cycles(1000000);



		// display servo angle
		uart_putc("Servo: ", 7);

		while(1){

			// read in value from ADC
			ADC10CTL0 |= ENC + ADC10SC;             // Sampling and conversion start
			__bis_SR_register(CPUOFF + GIE);        // LPM0, ADC10_ISR will force exit
			ADCvalue = ADC10MEM;					// grab value
			updateADCDisplay();						// display ADC value for debugging

			// update servo angle
			servoAngle = ADC10MEM * .1885;

			// LED debugging
			/*
			if (servoAngle >= 179){ // 180 deg: turn red LED on
				P1OUT |= BIT0;
				P1OUT &= ~BIT6;
				TA0CCR1 = 2000;         // set duty cycle to 2/20 (10%)
			} else if (servoAngle == 90){ // 90 deg: turn both LEDs on
				P1OUT |= BIT0;
				P1OUT |= BIT6;
				TA0CCR1 = 1500;         // set duty cycle to 15/200 (7.5%)
			} else if(servoAngle == 0){ // 0  deg: turn green LED on
				P1OUT |= BIT6;
				P1OUT &= ~BIT0;
				TA0CCR1 = 180;         // set duty cycle to 1/20 (5%)
			}*/

			/* send PWM signal to servo based on ADC value
		    | 180 * 11.1111 + 500 = ~2500 (~12.5% duty cycle)
		    | 90 * 11.1111 + 500 = ~1500 (~7.5% duty cycle)
		    | 0 * 11.1111 + 500 = (~2.5% duty cycle)
		    */
			TA0CCR1 = (servoAngle * 11.1111) + 500;

			// update the LCD display of the servo angle
			updateServoAngleDisplay();

		}

}

void updateServoAngleDisplay(){
	// get ascii of degree
	itoa(servoAngle, result, 10);

	// send control character
	uart_putcc(0xFE); // 241
	uart_putcc(0x87); // place cursor at position 7

	if(servoAngle < 10){
		uart_putc(result, 1);
	} else if(servoAngle < 99){
		uart_putc(result, 2);
	} else {
		uart_putc(result, 3);
	}

	// add on units
	uart_putc(" deg   ", 7);
}

void updateADCDisplay(){
	// get ascii of degree
	itoa(ADCvalue, ADCresult, 16);

	// send control character
	uart_putcc(0xFE); // 241
	uart_putcc(0x94); // place cursor at position 20

	if(ADCvalue < 0xF){
		uart_putc(ADCresult, 1);
		uart_putc("  ", 2);
	} else if(ADCvalue < 0xFF){
		uart_putc(ADCresult, 2);
		uart_putc(" ", 1);
	} else {
		uart_putc(ADCresult, 3);
	}
}

// wait for TX to be ready for sending
void uart_putc(const char * c, const int8_t len){
    int i;
    for (i = 0; i < len; i++){
    	while (!(IFG2 & UCA0TXIFG));  // USCI_A0 TX buffer ready?
    	UCA0TXBUF = c[i];             // TX
    }

}

// wait for TX to be ready for sending for control characters only
void uart_putcc(const int8_t i){
    while (!(IFG2 & UCA0TXIFG));  // USCI_A0 TX buffer ready?

    UCA0TXBUF = i;           	  // TX
}

/**
Inter to asci
value: integer to convert, result: buffer to put the characters into, base: 10 if decimal
*/
void itoa(int16_t value, char* result, uint16_t base){
      // check that the base if valid
      if (base < 2 || base > 36) { *result = '\0';}

      char* ptr = result, *ptr1 = result, tmp_char;
      int16_t tmp_value;

      do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
      } while ( value );

      *ptr-- = '\0';
      while(ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
      }
}

// ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
  __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}
